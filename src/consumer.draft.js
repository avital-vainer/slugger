import log from "@ajar/marker";
import {slugger} from "./index.js";


function test(...args){
    let strings = ["hello this", "is my new", "blog post"];
    log.cyan("input: ", strings)
    log.magenta("output: ", slugger(...strings));
}

test();