

export function slugger(...strs){
    let slugged = [...strs].join(" ");
    return slugged.replaceAll(" ", "-");
}
